//
//  MainTabbar.swift
//  weibo
//
//  Created by Weefeng Ma on 16/7/18.
//  Copyright © 2016年 maweefeng. All rights reserved.
//

import UIKit

class MainTabbar: UITabBar {
    //默认的构造方面
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpUI()
    }
    //如果重写 init 系统默认这个控件是通过代码创建的
    required init?(coder aDecoder: NSCoder) {
        //只有默认报错的语句
        //如果有人通过 nib 或 sb 创建 程序就会崩溃
        super.init(coder: aDecoder)
        setUpUI()
    }
    private func setUpUI(){
        
        addSubview(composeBtn)
        
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        //手动修改按钮的位置
        //遍历所有的子视图
        let w = self.bounds.width/5
        let h = self.bounds.height
        let rect = CGRect(x:0,y:0,width: w,height: h)
        //定义增量变量
        var index:CGFloat = 0
        for subView in subviews {
            if subView.isKindOfClass(NSClassFromString("UITabBarButton")!) {//修改 frame
                subView.frame=CGRectOffset(rect, index*w, 0)
                index += index == 1 ? 2 : 1
            }
        }
        //修改按钮的位置
        composeBtn.frame=CGRectOffset(rect,w*2,0)
        bringSubviewToFront(composeBtn)
    }
    
    lazy var composeBtn: UIButton = {
        //uibutton 自定义的样式按钮
        let btn = UIButton()
        btn.setBackgroundImage(UIImage(named:"tabbar_compose_button"), forState: UIControlState.Normal)
        btn.setBackgroundImage(UIImage(named:"tabbar_compose_button_highlighted"), forState: UIControlState.Highlighted)
        btn.setImage(UIImage(named :"tabbar_compose_icon_add" ), forState: UIControlState.Normal)
        btn.setImage(UIImage(named :"tabbar_compose_icon_add_highlighted" ), forState: UIControlState.Highlighted)
        btn.sizeToFit()
        return btn
        
    }()
    /*
     // Only override drawRect: if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func drawRect(rect: CGRect) {
     // Drawing code
     }
     */
    
}
