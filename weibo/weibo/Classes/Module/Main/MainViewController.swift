//
//  MainViewController.swift
//  weibo
//
//  Created by Weefeng Ma on 16/7/17.
//  Copyright © 2016年 maweefeng. All rights reserved.
//

import UIKit

class MainViewController: UITabBarController {
    
    @objc private func composeDidClick(){
    print(__FUNCTION__)
    
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        let mainTabbar=MainTabbar()
        setValue(mainTabbar, forKey: "tabBar")
        
        addChildViewControllers()
        mainTabbar.composeBtn.addTarget( self, action:#selector(MainViewController.composeDidClick), forControlEvents: .TouchUpInside)
   
    }

    private func addChildViewControllers() {
        addChildViewController(HomeTableViewController(), title: "首页", imageNamed: "tabbar_home")
        addChildViewController(MessageTableViewController(), title: "消息", imageNamed: "tabbar_message_center")
        addChildViewController(DiscoverTableViewController(), title: "发现", imageNamed: "tabbar_discover")
        addChildViewController(ProfileTableViewController(), title: "我", imageNamed: "tabbar_profile")
    }
    private func addChildViewController(vc:UIViewController,title:String,imageNamed:String) {
        //设置颜色方法（1）
        self.tabBar.tintColor=UIColor.orangeColor()
      
        //实例化导航视图控制器
        let nav = UINavigationController(rootViewController : vc)
        //        nav.title="首页"
        //        title = "Home"
        vc.title = title
        vc.tabBarItem.image=UIImage(named: imageNamed)
        //设置颜色方法（2）复杂的设置图片和字体的样式
//        vc.tabBarItem.selectedImage=UIImage(named: imageNamed + "_highlighted")?.imageWithRenderingMode( UIImageRenderingMode.AlwaysOriginal)
//        vc.tabBarItem.setTitleTextAttributes([NSForegroundColorAttributeName : UIColor.orangeColor()], forState:UIControlState.Selected)
        addChildViewController(nav)
        
    }

    
}
